determiner([D], determinan(D)) :-
	member(D,[the, a, an, some]).

noun([N], ktbenda(N)) :-
	member(N,[dog, street, ball, bat, boy]).

adj([A], ktsifat(A)) :-
	member(A,[fast, little, big]).

prep([P], ktdepan(P)) :-
	member(P,[down, under]).

verb([V], ktkerja(V)) :-
	member(V,[ran, caught, yelled, see, saw]).

noun_phrase(NP,frasa_benda(NTree)) :-
	noun(NP,NTree).

noun_phrase(NP,frasa_benda(DTree,NTree)) :-
	determiner(D,DTree), 
	noun(N,NTree),append(D,N,NP).

prep_phrase(PP, frasa_prep(PTree, NPTree)) :-
	prep(P, PTree),
	noun_phrase(NP, NPTree),append(P,NP,PP).

verb_phrase(VP, frasa_kerja(VTree)) :-
	verb(VP, VTree).

verb_phrase(VP, frasa_kerja(VTree, NPTree)) :-
	verb(V, VTree),
	noun_phrase(NP, NPTree),append(V,NP,VP).

verb_phrase(VP, frasa_kerja(VTree, PPTree)) :-
	verb(V, VTree),
	prep_phrase(PP, PPTree),append(V,PP,VP).

sentence(S, kalimat(NPTree,VPTree) ) :-
	noun_phrase(NP,NPTree),
	verb_phrase(VP,VPTree), append(NP,VP,S).